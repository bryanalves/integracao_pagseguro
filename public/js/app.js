function manipularElemento(el)
{
    var div = document.getElementById(el).style.display;

    if (div == 'none') {
        document.getElementById(el).style.display = '';
    } else {
        document.getElementById(el).style.display = 'none';
    }
}

function pagar(pedido, preco) {
    $('#btnPagar').hide();
    $('#tituloPedido').html('Pedido nº ' + pedido);
    PagSeguroDirectPayment.getPaymentMethods({
        amount: preco,
        success: function (response) {
            if (response.paymentMethods.hasOwnProperty("BOLETO")) {
                var boleto = response.paymentMethods.BOLETO.options;
                var img = '<img src="https://stc.pagseguro.uol.com.br' + boleto.BOLETO.images.MEDIUM.path + '">';
                var alink = '<a href="#boleto" id="boleto" onclick="buscarLinkPagamento(' + pedido + ')">' + img + '</a>';
                $('#formas_pagamentos').html(alink);
            }
        },
        error: function (response) {
            alert("Formas de pagamento não encontrada");
        },
        complete: function (response) {

        }
    });
}

function buscarLinkPagamento(pedido)
{
    $('#msgAguarde').show();

    $.ajax({
        dataType: 'json',
        type: "GET",
        url: '/pagamento/pagseguro/boleto/' + pedido,
        success: function (data) {
            $('#msgAguarde').hide();
            $('#btnPagar').attr('href', data.url);
            $('#btnPagar').show();
            $('#boleto').attr('onclick', '');
        }
    });
}