<?php

function formatarData($data)
{
    $dateTime = new DateTime($data);
    
    return $dateTime->format('d/m/Y h:i:s');
}

function echo_r($param)
{
    echo "<pre>";
    print_r($param);
    echo "</pre>";
}
