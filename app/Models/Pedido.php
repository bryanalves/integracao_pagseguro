<?php

namespace App\Models;

use PDO;

class Pedido extends DB
{

    private $conn;

    public function __construct()
    {
        $this->conn = new DB();
    }

    public function buscarPedido($idPedido)
    {
        $sqlPedidos = "
        SELECT
            pedido.id, pedido.frete, pedido.total
        FROM
            pedido
        WHERE
            pedido.id = :codigoPedido";

        $pdo = $this->getConexao()->prepare($sqlPedidos);
        $pdo->bindParam('codigoPedido', $idPedido);
        $pdo->execute();

        $dados = $pdo->fetch(PDO::FETCH_ASSOC);

        return $dados;
    }

    public function buscarPedidos($limit, $status = '')
    {
        $and = '';

        if (!empty($status)) {
            $and = " AND status LIKE '%{$status}%'";
        }

        $sqlPedidos = "
        SELECT
            pedido.id, pedido.subtotal, pedido.frete, pedido.total, 
            pedido.status, pedido.tipo_pagamento, pedido.mensagem,
            pedido.created_at data
        FROM
            pedido         
        WHERE
            1=1
            $and
        LIMIT
            {$limit[0]}, {$limit[1]}";

        $pdo = $this->getConexao()->prepare($sqlPedidos);

        $pdo->execute();

        $dados = $pdo->fetchAll(PDO::FETCH_ASSOC);

        return $dados;
    }

    public function buscarItensPedido($idPedido)
    {
        $sqlItensPedido = "
        SELECT
            nome, preco
        FROM
            pedido_itens itens
        JOIN
            produto_filho produto
            ON
                itens.id_produto = produto.id
        WHERE
            itens.id_pedido = {$idPedido}";

        $pdo = $this->getConexao()->prepare($sqlItensPedido);

        $pdo->execute();

        $dados = $pdo->fetchAll(PDO::FETCH_ASSOC);

        return $dados;
    }

    public function buscarEnderecoEntregaPedido($idPedido)
    {

        $sqlEndereEntrega = "
        SELECT
            entrega.nome_destinatario, entrega.cep, entrega.endereco,
            entrega.numero,entrega.complemento,entrega.bairro,entrega.estado,
            entrega.cidade,entrega.referencia
        FROM
            pedido_endereco_entrega entrega
        WHERE
            entrega.id_pedido = {$idPedido}";

        $pdo = $this->getConexao()->prepare($sqlEndereEntrega);

        $pdo->execute();

        $dados = $pdo->fetch(PDO::FETCH_ASSOC);

        return $dados;
    }

}
