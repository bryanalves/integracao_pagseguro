<?php

namespace App\Models;

class PagSeguro
{

    public function getSessionIdPagSeguro()
    {
        $query = http_build_query(
                array(
                    'email' => 'suporte@lojamodelo.com.br',
                    'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
                )
        );

        $options = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
            )
        );

        $streamContext = stream_context_create($options);

        $sessionPagSeguro = @file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/sessions?' . $query, false, $streamContext);

        $xmlSessionId = simplexml_load_string($sessionPagSeguro);

        return $xmlSessionId->id;
    }

    public function boleto($pedido)
    {

        $dadosBoleto = [
            'email' => 'suporte@lojamodelo.com.br',
            'token' => '57BE455F4EC148E5A54D9BB91C5AC12C',
            'paymentMode' => 'default',
            'paymentMethod' => 'boleto',
            'receiverEmail' => 'suporte@lojamodelo.com.br',
            'notificationURL' => 'https://sualoja.com.br/notifica.html',
            'reference' => "REF{$pedido['id']}",
            'senderName' => $pedido['entrega']['nome_destinatario'],
            'senderCPF' => '52795765659',
            'senderAreaCode' => '13',
            'senderPhone' => '33333333',
            'senderEmail' => 'carlos@sandbox.pagseguro.com.br',
            'shippingAddressStreet' => $pedido['entrega']['endereco'],
            'shippingAddressNumber' => $pedido['entrega']['numero'],
            'shippingAddressComplement' => $pedido['entrega']['complemento'],
            'shippingAddressDistrict' => $pedido['entrega']['bairro'],
            'shippingAddressPostalCode' => $pedido['entrega']['cep'],
            'shippingAddressCity' => $pedido['entrega']['cidade'],
            'shippingAddressState' => $pedido['entrega']['estado'],
            'shippingAddressCountry' => 'BRA',
            'currency' => 'BRL',
            'extraAmount' => "{$pedido['total']}",
            'shippingType' => '1',
            'shippingCost' => "{$pedido['frete']}",
        ];

        $dadosBoleto["itemId1"] = $pedido['id'];
        $dadosBoleto["itemDescription1"] = "Pedido nº {$pedido['id']}";
        $dadosBoleto["itemAmount1"] = "{$pedido['total']}";
        $dadosBoleto["itemQuantity1"] = 1;

        $query = http_build_query($dadosBoleto);

        $options = array('http' =>
            array(
                'header' => "Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1\r\n" .
                "Content-Length: " . strlen($query) . "\r\n" .
                "User-Agent:MyAgent/1.0\r\n",
                'method' => "POST",
                'content' => $query,
                'ignore_errors' => true,
            )
        );

        $streamContext = stream_context_create($options);
        $resultadoPagSeguro = @file_get_contents('https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/', false, $streamContext);

        $result = simplexml_load_string($resultadoPagSeguro);

        return (string) $result->paymentLink;
    }

}
