<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\PagSeguro;

class PedidoController
{

    public function index($pagina, $status = '')
    {

        $objPedido = new Pedido();
        $objPagSeguro = new PagSeguro();
        
        $limit = [$pagina * 10, 10];

        $pedidos = $objPedido->buscarPedidos($limit, $status);

        foreach ($pedidos as $key => $pedido) {
            $pedidos[$key]['produtos'] = $objPedido->buscarItensPedido($pedido['id']);            
        }

        $dados['pedidos'] = $pedidos;
        $dados['sessionId'] = $objPagSeguro->getSessionIdPagSeguro();
        
        return view('index', $dados);
    }

    public function status($status, $pagina)
    {
        $objPedido = new Pedido();
        $objPagSeguro = new PagSeguro();
        
        $limit = [$pagina * 10, 10];

        $pedidos = $objPedido->buscarPedidos($limit, $status);

        foreach ($pedidos as $key => $pedido) {
            $pedidos[$key]['produtos'] = $objPedido->buscarItensPedido($pedido['id']);
        }

        $dados['pedidos'] = $pedidos;
        $dados['sessionId'] = $objPagSeguro->getSessionIdPagSeguro();
        
        return view('index', $dados);
    }

}
