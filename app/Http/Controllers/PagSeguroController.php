<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\PagSeguro;

class PagSeguroController
{

    public function boleto($idPedido)
    {
        $objPedido = new Pedido();
        $objPagSeguro = new PagSeguro();

        $pedido = $objPedido->buscarPedido($idPedido);

        $pedido['entrega'] = $objPedido->buscarEnderecoEntregaPedido($idPedido);

        $link = $objPagSeguro->boleto($pedido);

        return json_encode(['url' => $link]);
    }

}
