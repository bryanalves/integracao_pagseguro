<!doctype html>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/css/app.css">       

        <!-- PagSeguro -->
        <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
        <script type="text/javascript">
            window.sessionId = "<?= $sessionId; ?>";
            PagSeguroDirectPayment.setSessionId(window.sessionId);
        </script>
        <!-- PagSeguro -->

        <script src="/js/app.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <section class="content">
                    <h1>Lista de pedidos</h1>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <a class="btn btn-success"href="/pedidos/aprovado/0">Aprovado</a>
                                        <a class="btn btn-warning"href="/pedidos/processando/0">Processando</a>
                                        <a class="btn btn-danger"href="/pedidos/reprovado/0">Reprovado</a>
                                        <a class="btn btn-default"href="/pedidos/0">Todos</a>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table">
                                        <tbody>
                                            <?php foreach ($pedidos as $pedido) { ?>
                                                <tr style="cursor: default;">
                                                    <td>
                                                        <div class="media">
                                                            <div class="media-body">
                                                                <span class="media-meta pull-right">
                                                                    <?= formatarData($pedido['data']); ?>
                                                                </span>
                                                                <h4 class="title">
                                                                    Pedido - nº <?= $pedido['id']; ?>
                                                                    <span class="pull-right <?= strtolower($pedido['status']); ?>">
                                                                        (<?= ucwords(strtolower($pedido['status'])); ?>)
                                                                    </span>
                                                                </h4>

                                                                <span style="float:right;cursor:pointer" onclick="manipularElemento('produtos_<?= $pedido['id']; ?>')" >
                                                                    Ver produtos
                                                                </span>
                                                                <p class="summary">
                                                                    <?= ucfirst(strtolower($pedido['tipo_pagamento'])); ?> -
                                                                    <?= 'R$' . number_format($pedido['total'], 2, ',', '.'); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="produtos_<?= $pedido['id']; ?>" style="display:none">
                                                    <td>
                                                        <div class="media">
                                                            <div class="media-body">
                                                                <table class="table table-bordered table-condensed table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Nome</th>
                                                                            <th>Preço
                                                                                <span style="float:right">
                                                                                    <a href="#" onclick="pagar(<?= $pedido['id']; ?>,<?= $pedido['total']; ?>)" data-toggle="modal" data-target=".bs-pagamento-modal-md">
                                                                                        Pagar
                                                                                    </a>
                                                                                </span>
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($pedido['produtos'] as $produto) { ?>
                                                                            <tr>
                                                                                <td><?= $produto['nome']; ?></td>
                                                                                <td>
                                                                                    <?= 'R$ ' . number_format($produto['preco'], 2, ',', '.'); ?>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>                                   
                                </div>
                            </div>
                        </div>                
                    </div>
                </section>
            </div>
        </div>        
        <div class="modal fade bs-pagamento-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="text-align:center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Formas de Pagamento</h4>
                        <h5 id="tituloPedido" class="modal-title"></h5>
                    </div>
                    <div class="modal-body">
                        <div id="formas_pagamentos">

                        </div>
                        <div style="text-align:right" id="divBtnPagar">
                            <span id="msgAguarde" style="display:none;color:red">Aguarde...</span>
                            <a href="" id="btnPagar" target="_blank" class="btn btn-success" style="display:none">Pagar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

