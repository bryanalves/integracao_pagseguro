<?php

$router->get('/pedidos/{pagina}', ['uses' => 'PedidoController@index']);
$router->get('/pedidos/{status}/{pagina}', ['uses' => 'PedidoController@status']);
$router->get('/pagamento/pagseguro/boleto/{pedido}', ['uses' => 'PagSeguroController@boleto']);

$router->get('/pedidos', function () {
    return redirect('pedidos/0');
});

$router->get('/', function () {
    return redirect('pedidos/0');
});
